{-# OPTIONS --rewriting #-}
open import Relation.Binary.PropositionalEquality hiding ([_])
open import Agda.Builtin.Equality.Rewrite
open ≡-Reasoning

-- categorical syntax
infixl 7 wk
infixl 7 wkt
infixl 7 sub
infixl 7 subt
infixl 7 _[_]
infixl 5 _,_
infixl 5 _,'_
infixl 8 _+_
infixl 9 _▷_
infixr 10 _∘_
infixr 11 _⇒_


data Ty : Set where
  ι : Ty
  _⇒_ : Ty → Ty → Ty
variable
  A B C : Ty

data Con : Set where
  ∙ : Con
  _▷_ : Con → Ty → Con
variable
  Γ Δ Θ : Con

data Sub : Con → Con → Set
data Tm  : Con → Ty → Set

data Sub where
  id : Sub Γ Γ
  _∘_ : Sub Θ Δ → Sub Γ Θ → Sub Γ Δ
  ε : Sub Γ ∙
  _,_ : Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▷ A)
  p : Sub (Γ ▷ A) Γ
variable
  σ δ ν : Sub Γ Δ

data Tm where
  _[_] : Tm Δ A → Sub Γ Δ → Tm Γ A
  q : Tm (Γ ▷ A) A
  lam : Tm (Γ ▷ A) B → Tm Γ (A ⇒ B)
  app : Tm Γ (A ⇒ B) → Tm (Γ ▷ A) B
variable
  t u v : Tm Γ A

postulate
  ass   : (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
  idl   : id ∘ σ ≡ σ
  idr   : σ ∘ id ≡ σ
  ∙η    : {σ : Sub Γ ∙} → σ ≡ ε

  ▷β₁   : p ∘ (σ , t) ≡ σ
  ▷β₂   : q [ σ , t ] ≡ t
  ▷η    : (p , q {Γ} {A}) ≡ id
  ,∘    : (σ , t) ∘ δ ≡ ((σ ∘ δ) , (t [ δ ]))
  [id]  : t [ id ] ≡ t
  [∘]   : t [ σ ∘ δ ] ≡ t [ σ ] [ δ ]

  ⇒β    : app (lam t) ≡ t
  ⇒η    : lam (app t) ≡ t
  lam[] : (lam t) [ σ ] ≡ lam (t [(σ ∘ p) , q ])

-- derivable:
app[] : (app t) [ (σ ∘ p) , q ] ≡ app (t [ σ ])
app[] {Γ} {A} {B} {t} {Δ} {σ} = begin
  app t [ σ ∘ p , q ]             ≡⟨ sym (⇒β {t = app t [ σ ∘ p , q ]}) ⟩
  app (lam (app t [ σ ∘ p , q ])) ≡⟨ cong (λ x → app x) (sym (lam[] {t = app t}{σ = σ})) ⟩
  app (lam (app t) [ σ ])         ≡⟨ cong (λ x → app (x [ σ ])) (⇒η {t = t}) ⟩
  app (t [ σ ]) ∎
-- deriving the telescopic syntax

_+_ : Con → Con → Con
Γ + ∙ = Γ
Γ + (Δ ▷ A) = (Γ + Δ) ▷ A

wk : (Δ : Con) → Sub ((Γ ▷ B) + Δ) (Γ + Δ)
wk ∙ = p
wk (Δ ▷ A) = (wk Δ ∘ p) , q

sub : (Δ : Con) → Tm Γ B → Sub  (Γ + Δ)  ((Γ ▷ B) + Δ)
sub ∙ u = id , u
sub (Δ ▷ A) u = (sub Δ u ∘ p) , q

wksub : (Δ : Con) → wk Δ ∘ sub Δ u ≡ id
wksub ∙ = ▷β₁
wksub {Γ}{B}{u} (Δ ▷ A) =
                                           begin
  (((wk Δ ∘ p) , q) ∘ ((sub Δ u ∘ p) , q))
                                           ≡⟨ ,∘ ⟩
  ((((wk Δ ∘ p) ∘ ((sub Δ u ∘ p) , q)) , (q [ (sub Δ u ∘ p) , q ])))
                                           ≡⟨ cong (λ x → (x , (q [ (sub Δ u ∘ p) , q ]))) ass ⟩
  (((wk Δ ∘ (p ∘ ((sub Δ u ∘ p) , q))) , (q [ (sub Δ u ∘ p) , q ])))
                                           ≡⟨ cong (λ x → ((wk Δ ∘ x) , (q [ (sub Δ u ∘ p) , q ]))) ▷β₁ ⟩
  ((wk Δ ∘ (sub Δ u ∘ p)) , (q [ (sub Δ u ∘ p) , q ]))
                                           ≡⟨ sym (cong (λ x → (x , (q [ (sub Δ u ∘ p) , q ]))) ass)   ⟩
  (((wk Δ ∘ sub Δ u) ∘ p) , (q [ (sub Δ u ∘ p) , q ]))
                                           ≡⟨  cong (λ x → ((x ∘ p) , (q [ (sub Δ u ∘ p) , q ]))) (wksub Δ) ⟩
  ((id ∘ p) , (q [ (sub Δ u ∘ p) , q ]))
                                           ≡⟨  cong (λ x → (x , (q [ (sub Δ u ∘ p) , q ]))) idl  ⟩
  (p , (q [ (sub Δ u ∘ p) , q ]))
                                           ≡⟨  cong (λ x → (p , x))  ▷β₂ ⟩
  (p , q)
                                           ≡⟨  ▷η ⟩

  id
                                           ∎

syntax wkt Γ Δ A t = t ⟨ Γ , A , Δ ⟩
wkt : (Γ : Con) → (Δ : Con) → (B : Ty) → Tm (Γ + Δ) A → Tm ((Γ ▷ B) + Δ) A
t ⟨ Γ , B , Δ ⟩ = t [ wk Δ ]

syntax subt Γ Δ t u = t ⌈ Γ , u , Δ ⌉
subt : (Γ : Con) → (Δ : Con) → Tm ((Γ ▷ B) + Δ) A → Tm Γ B → Tm (Γ + Δ) A
t ⌈ Γ , u , Δ ⌉  = t [ sub Δ u ]


--TODO: move to common file
idl+ : ∙ + Γ ≡ Γ
idl+ {∙}     = refl
idl+ {Γ ▷ A} = cong (λ z → z ▷ A ) (idl+ {Γ})
{-# REWRITE idl+ #-}

idr+ : Γ + ∙ ≡ Γ
idr+ = refl

ass▷+ : Γ + (∙ ▷ A + Δ) ≡ Γ ▷ A + Δ
ass▷+ {Γ} {A} {∙}     = refl
ass▷+ {Γ} {A} {Δ ▷ B} = cong (λ z → z ▷ B) (ass▷+ {Γ}{A}{Δ})
{-# REWRITE ass▷+ #-}

ass+ : Γ + (Δ + Θ) ≡ (Γ + Δ) + Θ
ass+ {Θ = ∙} = refl
ass+ {Γ}{Δ}{Θ = Θ ▷ A} = cong (λ x → x ▷ A) (ass+ {Γ}{Δ}{Θ})
{-# REWRITE ass+ #-}

q[∙]   : q ⌈ Γ , u , ∙ ⌉     ≡ u
q[mid] : q ⌈ Γ , u , Δ ▷ A ⌉ ≡ q
q⟨mid⟩ : q ⟨ Γ , A , Δ ▷ B ⟩  ≡ q

⟨⟩⟨⟩   : t ⟨ Γ , A , Δ     + Θ ⟩ ⟨ Γ ▷ A + Δ , B               , Θ ⟩  ≡ t ⟨ Γ     + Δ , B , Θ ⟩ ⟨ Γ , A , (Δ ▷ B) + Θ ⟩
⟨⟩⌈⌉   : t ⟨ Γ , A , Δ ▷ B + Θ ⟩ ⌈ Γ ▷ A + Δ , u ⟨ Γ , A , Δ ⟩ , Θ ⌉ ≡ t ⌈ Γ     + Δ , u , Θ ⌉ ⟨ Γ , A , Δ + Θ ⟩
⌈⌉⟨⟩  : t ⟨ Γ ▷ A + Δ , B , Θ ⟩ ⌈ Γ , u , Δ ▷ B + Θ ⌉ ≡ t ⌈ Γ , u , Δ + Θ ⌉ ⟨ Γ + Δ , B , Θ ⟩
⟨⌈⌉⟩     : t ⟨ Γ , A , Δ ⟩ ⌈ Γ , u , Δ ⌉ ≡ t
⟨,⌉    : t ⟨ Γ , A , ∙ ▷ A + Δ ⟩ ⌈ Γ ▷ A , q , Δ ⌉ ≡ t
⌈⌉⌈⌉   : {t : Tm (Γ ▷ A + Δ ▷ B + Θ) C} →
         t ⌈ Γ , u , Δ ▷ B + Θ ⌉ ⌈ Γ + Δ , v ⌈ Γ , u , Δ ⌉ , Θ ⌉ ≡ t ⌈ Γ ▷ A + Δ , v , Θ ⌉ ⌈ Γ , u , Δ + Θ ⌉


⇒β'    : app (lam t) ≡ t
⇒η'    : lam (app t) ≡ t
lam⟨⟩  : lam t ⟨ Γ , C , Δ ⟩      ≡ lam (t ⟨ Γ , C , Δ ▷ A ⟩)
lam⌈⌉ : lam t ⌈ Γ , u , Δ ⌉     ≡ lam (t ⌈ Γ , u , Δ ▷ A ⌉)
app⟨⟩  : app t ⟨ Γ , C , Δ ▷ A ⟩  ≡ app (t ⟨ Γ , C , Δ ⟩) 
app⌈⌉ : app t ⌈ Γ , u , Δ ▷ A ⌉ ≡ app (t ⌈ Γ , u , Δ ⌉)

--helpers
∘p,q∘ : (σ ∘ p , q {A = A}) ∘ (δ ∘ ν , u) ≡ ((σ ∘ δ) ∘ ν) , u
∘p,q∘ {_} {_} {σ} {A} {_} {δ} {_} {ν} {u} = begin
  (σ ∘ p , q) ∘ (δ ∘ ν , u)                 ≡⟨ ,∘ {σ = σ ∘ p}{t = q}{δ = δ ∘ ν , u} ⟩
  ((σ ∘ p) ∘ (δ ∘ ν , u)) , q [ δ ∘ ν , u ] ≡⟨ cong (λ x → ((σ ∘ p) ∘ (δ ∘ ν , u)) , x) (▷β₂ {σ = δ ∘ ν}{t = u})⟩
  ((σ ∘ p) ∘ (δ ∘ ν , u)) , u               ≡⟨ cong (λ x → x , u) (ass {σ = σ}{δ = p}{ν = δ ∘ ν , u}) ⟩
  (σ ∘ (p ∘ (δ ∘ ν , u))) , u               ≡⟨ cong (λ x → σ ∘ x , u) (▷β₁ {σ = δ ∘ ν}{t = u}) ⟩
  (σ ∘ (δ ∘ ν)) , u                         ≡⟨ cong (λ x → x , u) (sym (ass {σ = σ}{δ = δ}{ν = ν})) ⟩
  ((σ ∘ δ) ∘ ν) , u ∎

wk∘subid : {u : Tm Γ B} → wk Δ ∘ sub Δ u ≡ id
wk∘subid {Γ} {B} {∙}     {u} = ▷β₁ {σ = id}{t = u}
wk∘subid {Γ} {B} {Δ ▷ A} {u} = begin
 (wk Δ ∘ p , q) ∘ (sub Δ u ∘ p , q)  ≡⟨ ∘p,q∘ {σ = wk Δ}{δ = sub Δ u}{u = q} ⟩
 ((wk Δ ∘ sub Δ u) ∘ p) , q          ≡⟨ cong (λ x → x ∘ p , q) (wk∘subid {Γ}{B}{Δ}{u}) ⟩
 id ∘ p , q                          ≡⟨ cong (λ x → x , q) (idl {σ = p}) ⟩
 p , q                               ≡⟨ ▷η ⟩ 
 id ∎

wk∘sub' : wk {Γ} (∙ ▷ A + Δ) ∘ sub Δ q ≡ id
wk∘sub' {Γ} {A} {∙}     = begin
  (p ∘ p , q) ∘ (id , q)      ≡⟨ cong (λ x → (p ∘ p , q) ∘ (x , q)) (sym (idl {σ = id})) ⟩
  (p ∘ p , q) ∘ (id ∘ id , q) ≡⟨ ∘p,q∘ {σ = p}{δ = id}{u = q} ⟩
  (p ∘ id) ∘ id , q           ≡⟨ cong (λ x → x , q) (idr {σ = p ∘ id}) ⟩
  p ∘ id , q                  ≡⟨ cong (λ x → x , q) (idr {σ = p}) ⟩
  p , q                       ≡⟨ ▷η ⟩
  id ∎
wk∘sub' {Γ} {A} {Δ ▷ B} = begin
  (wk (∙ ▷ A + Δ) ∘ p , q) ∘ (sub Δ q ∘ p , q) ≡⟨ ∘p,q∘ {σ = wk (∙ ▷ A + Δ)}{δ = sub Δ q}{u = q} ⟩
  (wk (∙ ▷ A + Δ) ∘ sub Δ q) ∘ p , q           ≡⟨ cong (λ x → x ∘ p , q) (wk∘sub' {Γ}{A}{Δ}) ⟩
  id ∘ p , q                                    ≡⟨ cong (λ x → x , q) (idl {σ = p}) ⟩
  p , q                                         ≡⟨ ▷η ⟩
  id ∎

wk∘wk : wk {Γ}{B} (Δ + Θ) ∘ wk {_}{C} Θ ≡ wk {_}{C} Θ ∘ wk (Δ ▷ C + Θ)
wk∘wk {Γ} {B} {Δ} {∙}     = sym (▷β₁ {σ = wk Δ ∘ p}{t = q})
wk∘wk {Γ} {B} {Δ} {Θ ▷ A} {C} = begin
  (wk (Δ + Θ) ∘ p , q) ∘ (wk Θ ∘ p , q)     ≡⟨ ∘p,q∘ {σ = wk (Δ + Θ)}{δ = wk Θ } ⟩
  (wk (Δ + Θ) ∘ wk Θ) ∘ p , q               ≡⟨ cong (λ x → x ∘ p , q) (wk∘wk {Γ}{B}{Δ}{Θ}) ⟩
  (wk Θ ∘ wk (Δ ▷ C + Θ)) ∘ p , q           ≡⟨ sym (∘p,q∘ {σ = wk Θ}{δ = wk (Δ ▷ C + Θ)}) ⟩
  (wk Θ ∘ p , q) ∘ (wk (Δ ▷ C + Θ) ∘ p , q) ∎

wk∘sub : {u : Tm (Γ + Δ) B} → wk {Γ}{C} (Δ ▷ B + Θ) ∘ sub Θ (u [ wk Δ ]) ≡ sub Θ u ∘ wk {_}{C} (Δ + Θ)
wk∘sub {Γ} {Δ} {B} {C} {∙} {u} = begin
  (wk Δ ∘ p , q) ∘ (id , u [ wk Δ ])      ≡⟨ cong (λ x → (wk Δ ∘ p , q) ∘ (x , u [ wk Δ ])) (sym (idr {σ = id})) ⟩
  (wk Δ ∘ p , q) ∘ (id ∘ id , u [ wk Δ ]) ≡⟨ ∘p,q∘  {σ = wk Δ}{δ = id}{ν = id}{u = u [ wk Δ ]} ⟩
  (wk Δ ∘ id) ∘ id , u [ wk Δ ]           ≡⟨ cong (λ x → x , u [ wk Δ ]) (idr {σ = wk Δ ∘ id}) ⟩
  wk Δ ∘ id , u [ wk Δ ]                  ≡⟨ cong (λ x → x , u [ wk Δ ]) (idr {σ = wk Δ}) ⟩
  wk Δ , u [ wk Δ ]                       ≡⟨ cong (λ x → x , u [ wk Δ ]) (sym (idl {σ = wk Δ})) ⟩
  id ∘ wk Δ , u [ wk Δ ]                  ≡⟨ sym (,∘ {σ = id}{t = u}{δ = wk Δ}) ⟩
  (id , u) ∘ wk Δ ∎
wk∘sub {Γ} {Δ} {B} {C} {Θ ▷ A} {u} = begin
  (wk (Δ ▷ B + Θ) ∘ p , q) ∘ (sub Θ (u [ wk Δ ]) ∘ p , q) ≡⟨ ∘p,q∘ {σ = wk (Δ ▷ B + Θ)}{δ = sub Θ (u [ wk Δ ])}{ν = p}{u = q} ⟩
  (wk (Δ ▷ B + Θ) ∘ sub Θ (u [ wk Δ ])) ∘ p , q           ≡⟨ cong (λ x → x ∘ p , q) (wk∘sub {Γ}{Δ}{B}{C}{Θ}{u}) ⟩
  (sub Θ u ∘ wk (Δ + Θ)) ∘ p , q                          ≡⟨ sym (∘p,q∘ {σ = sub Θ u}{δ = wk (Δ + Θ)}{u = q}) ⟩
  (sub Θ u ∘ p , q) ∘ (wk (Δ + Θ) ∘ p , q) ∎

wk∘subl : {u : Tm Γ A} → wk Θ ∘ sub (Δ ▷ B + Θ) u ≡ sub (Δ + Θ) u ∘ wk Θ
wk∘subl {Γ} {A} {∙}     {Δ} {B} {u} = ▷β₁ {σ = sub Δ u ∘ p}{t = q}
wk∘subl {Γ} {A} {Θ ▷ C} {Δ} {B} {u} = begin
  (wk Θ ∘ p , q) ∘ (sub (Δ ▷ B + Θ) u ∘ p , q) ≡⟨ ∘p,q∘ {σ = wk Θ}{δ = sub (Δ ▷ B + Θ) u}{ν = p}{u = q} ⟩
  (wk Θ ∘ sub (Δ ▷ B + Θ) u) ∘ p , q           ≡⟨ cong (λ x → x ∘ p , q) (wk∘subl {Γ}{A}{Θ}{Δ}{B}{u}) ⟩
  (sub (Δ + Θ) u ∘ wk Θ) ∘ p , q               ≡⟨ sym (∘p,q∘ {σ = sub (Δ + Θ) u}{δ = wk Θ}{u = q}) ⟩
  (sub (Δ + Θ) u ∘ p , q) ∘ (wk Θ ∘ p , q) ∎

sub∘sub : {u : Tm Γ A} → sub (Δ ▷ B + Θ) u ∘ sub Θ (v [ sub Δ u ]) ≡ sub Θ v ∘ sub (Δ + Θ) u
sub∘sub {Γ} {A} {Δ} {B} {∙}     {v} {u} = begin
  (sub Δ u ∘ p , q) ∘ (id , v [ sub Δ u ])      ≡⟨ cong (λ x → (sub Δ u ∘ p , q) ∘ (x , v [ sub Δ u ])) (sym (idr {σ = id})) ⟩
  (sub Δ u ∘ p , q) ∘ (id ∘ id , v [ sub Δ u ]) ≡⟨ ∘p,q∘ {σ = sub Δ u}{δ = id}{ν = id}{u = v [ sub Δ u ]} ⟩
  (sub Δ u ∘ id) ∘ id , v [ sub Δ u ]           ≡⟨ cong (λ x → x , v [ sub Δ u ]) (idr {σ = sub Δ u ∘ id}) ⟩
  sub Δ u ∘ id , v [ sub Δ u ]                  ≡⟨ cong (λ x → x , v [ sub Δ u ]) (idr {σ = sub Δ u}) ⟩
  sub Δ u , v [ sub Δ u ]                       ≡⟨ cong (λ x → x , v [ sub Δ u ]) (sym (idl {σ = sub Δ u}))  ⟩
  id ∘ sub Δ u , v [ sub Δ u ]                  ≡⟨ sym (,∘ {σ = id}{t = v}{δ = sub Δ u}) ⟩
  (id , v) ∘ sub Δ u ∎
sub∘sub {Γ} {A} {Δ} {B} {Θ ▷ C} {v} {u} = begin
  (sub (Δ ▷ B + Θ) u ∘ p , q) ∘ (sub Θ (v [ sub Δ u ]) ∘ p , q) ≡⟨ ∘p,q∘ {σ = sub (Δ ▷ B + Θ) u}{δ = sub Θ (v [ sub Δ u ])}{u = q} ⟩
  (sub (Δ ▷ B + Θ) u ∘ sub Θ (v [ sub Δ u ])) ∘ p , q           ≡⟨ cong (λ x → x ∘ p , q) (sub∘sub {Γ}{A}{Δ}{B}{Θ}{v}{u}) ⟩
  (sub Θ v ∘ sub (Δ + Θ) u) ∘ p , q                             ≡⟨ sym (∘p,q∘ {σ = sub Θ v}{δ = sub (Δ + Θ) u}{u = q}) ⟩
  (sub Θ v ∘ p , q) ∘ (sub (Δ + Θ) u ∘ p , q) ∎

--end of helpers

q[∙] {Γ} {A} {u} = begin
 q ⌈ Γ , u , ∙ ⌉ ≡⟨ refl ⟩
 q [ id , u ]     ≡⟨ ▷β₂ ⟩
 u ∎

q[mid] {Γ} {A} {u} {Δ} {B} = begin
  q ⌈ Γ , u , Δ ▷ B ⌉    ≡⟨ refl ⟩
  q [ (sub Δ u ∘ p) , q ] ≡⟨ ▷β₂ ⟩
  q ∎

q⟨mid⟩ {Γ} {A} {Δ} {B} = begin
  q ⟨ Γ , A , Δ ▷ B ⟩  ≡⟨ refl ⟩
  q [ (wk Δ ∘ p) , q ] ≡⟨ ▷β₂ ⟩
  q ∎

⟨⟩⟨⟩ {C} {Γ} {Δ} {Θ} {t} {A} {B} = begin
  t ⟨ Γ , A , Δ + Θ ⟩ ⟨ Γ ▷ A + Δ , B , Θ ⟩ ≡⟨ refl ⟩
  t [ wk (Δ + Θ) ] [ wk Θ ]                  ≡⟨ sym [∘] ⟩
  t [ wk (Δ + Θ) ∘ wk Θ ]                    ≡⟨ cong (λ x → t [ x ]) (wk∘wk {B = A}{Δ = Δ}{Θ = Θ}{C = B}) ⟩
  t [ wk Θ ∘ wk (Δ ▷ B + Θ) ]                ≡⟨ [∘] ⟩
  t [ wk Θ ] [ wk (Δ ▷ B + Θ) ]              ≡⟨ refl ⟩
  t ⟨ Γ + Δ , B , Θ ⟩ ⟨ Γ , A , Δ ▷ B + Θ ⟩ ∎

⟨⟩⌈⌉ {C} {B} {Γ} {Δ} {Θ} {t} {A} {u} = begin
  t ⟨ Γ , A , Δ ▷ B + Θ ⟩ ⌈ Γ ▷ A + Δ , u ⟨ Γ , A , Δ ⟩ , Θ ⌉ ≡⟨ refl ⟩
  t [ wk (Δ ▷ B + Θ) ] [ sub Θ (u [ wk Δ ]) ]                  ≡⟨ sym [∘] ⟩
  t [ wk (Δ ▷ B + Θ) ∘ sub Θ (u [ wk Δ ]) ]                    ≡⟨ cong (λ x → t [ x ]) (wk∘sub {Γ}{Δ}{_}{_}{Θ}{u}) ⟩
  t [ sub Θ u ∘ wk (Δ + Θ) ]                                   ≡⟨ [∘] ⟩
  t [ sub Θ u ] [ wk (Δ + Θ) ]                                 ≡⟨ refl ⟩
  t ⌈ Γ + Δ , u , Θ ⌉ ⟨ Γ , A , Δ + Θ ⟩ ∎

⌈⌉⟨⟩ {_} {A} {Γ} {Δ} {Θ} {t} {u} {B} = begin
  t ⟨ Γ ▷ A + Δ , B , Θ ⟩ ⌈ Γ , u , Δ ▷ B + Θ ⌉ ≡⟨ refl ⟩
  t [ wk Θ ] [ sub (Δ ▷ B + Θ) u ]                ≡⟨ sym [∘] ⟩
  t [ wk Θ ∘ sub (Δ ▷ B + Θ) u ]                  ≡⟨ cong (λ x → t [ x ]) (wk∘subl {_}{_}{Θ}{Δ}{B}{u}) ⟩
  t [ sub (Δ + Θ) u ∘ wk Θ ]                      ≡⟨ [∘] ⟩
  t [ sub (Δ + Θ) u ] [ wk Θ ]                    ≡⟨ refl ⟩
  t ⌈ Γ , u , Δ + Θ ⌉ ⟨ Γ + Δ , B , Θ ⟩ ∎

⟨⌈⌉⟩ {_} {Γ} {Δ} {t} {A} {u} = begin
  t ⟨ Γ , A , Δ ⟩ ⌈ Γ , u , Δ ⌉ ≡⟨ refl ⟩
  t [ wk Δ ] [ sub Δ u ]         ≡⟨ sym [∘] ⟩
  t [ wk Δ ∘ sub Δ u ]           ≡⟨ cong (λ x → t [ x ]) (wk∘subid {Δ = Δ}{u = u}) ⟩
  t [ id ]                       ≡⟨ [id] ⟩
  t ∎

⟨,⌉ {_} {Γ} {A} {Δ} {t} = begin
  subt (Γ ▷ A) Δ (wkt Γ (∙ ▷ A + Δ) A t) q ≡⟨ refl ⟩
  t [ wk (∙ ▷ A + Δ) ] [ sub Δ q ]         ≡⟨ sym [∘] ⟩
  t [ wk (∙ ▷ A + Δ) ∘ sub Δ q ]           ≡⟨ cong (λ x → t [ x ]) (wk∘sub' {_}{A}{Δ}) ⟩
  t [ id ]                                  ≡⟨ [id] ⟩
  t ∎

⌈⌉⌈⌉ {Γ} {A} {Δ} {B} {Θ} {C} {u} {v} {t} = begin
  t ⌈ Γ , u , Δ ▷ B + Θ ⌉ ⌈ Γ + Δ , v ⌈ Γ , u , Δ ⌉ , Θ ⌉ ≡⟨ refl ⟩
  t [ sub (Δ ▷ B + Θ) u ] [ sub Θ (v [ sub Δ u ]) ]           ≡⟨ sym [∘] ⟩
  t [ sub (Δ ▷ B + Θ) u ∘ sub Θ (v [ sub Δ u ]) ]             ≡⟨ cong (λ x → t [ x ]) (sub∘sub {Γ}{A}{Δ}{B}{Θ}{v}{u}) ⟩
  t [ sub Θ v ∘ sub (Δ + Θ) u ]                               ≡⟨ [∘] ⟩
  t [ sub Θ v ] [ sub (Δ + Θ) u ]                             ≡⟨ refl ⟩
  t ⌈ Γ ▷ A + Δ , v , Θ ⌉ ⌈ Γ , u , Δ + Θ ⌉ ∎

⇒β' = ⇒β

⇒η' = ⇒η

lam⟨⟩ {_}{Γ}{Δ}{A}{t}{C} = begin
  lam t ⟨ Γ , C , Δ ⟩      ≡⟨ refl ⟩
  lam t [ wk Δ ]           ≡⟨ lam[] ⟩
  lam (t [ wk Δ ∘ p , q ]) ≡⟨ refl ⟩
  lam (t ⟨ Γ , C , Δ ▷ A ⟩) ∎

lam⌈⌉ {_}{Γ}{_}{Δ}{A}{t}{u} = begin
  lam t ⌈ Γ , u , Δ ⌉        ≡⟨ refl ⟩
  lam t [ sub Δ u ]           ≡⟨ lam[] ⟩
  lam (t [ sub Δ u ∘ p , q ]) ≡⟨ refl ⟩
  lam (t ⌈ Γ , u , Δ ▷ A ⌉) ∎ 

app⟨⟩ {Γ}{Δ}{A}{B}{t}{C} = begin
  app t ⟨ Γ , C , Δ ▷ A ⟩ ≡⟨ refl ⟩
  app t [ wk Δ ∘ p , q ]  ≡⟨ app[] ⟩
  app (t [ wk Δ ])        ≡⟨ refl ⟩
  app (t ⟨ Γ , C , Δ ⟩)   ∎

app⌈⌉ {Γ} {_} {Δ} {A} {_} {t} {u} = begin
  app t ⌈ Γ , u , Δ ▷ A ⌉  ≡⟨ refl ⟩
  app t [ sub Δ u ∘ p , q ] ≡⟨ app[] ⟩
  app (t [ sub Δ u ])       ≡⟨ refl ⟩
  app (t ⌈ Γ , u , Δ ⌉)    ∎



--equ


wk↑ : (Δ Θ : Con) → Sub (Γ + Δ + Θ) (Γ + Θ)
wk↑ ∙       Θ = id
wk↑ (Δ ▷ A) ∙ = wk↑ Δ ∙ ∘ p
wk↑ {Γ} (Δ ▷ A) (Θ ▷ B) = wk↑ {Γ} (Δ ▷ A) Θ ∘ p , q

sub↑ : (Δ Θ : Con) → Sub Γ Δ → Sub (Γ + Θ) (Γ + Δ + Θ )
sub↑ ∙ Θ σ = id
sub↑ {Γ} (Δ ▷ A) ∙ σ  = sub↑ Δ ∙ (p ∘ σ) , q [ σ ]
sub↑ {Γ} (Δ ▷ A) (Θ ▷ B) σ = sub↑ (Δ ▷ A) Θ σ ∘ p , q

wk+ : (Γ Δ Θ : Con) → Tm (Γ + Θ) B → Tm (Γ + Δ + Θ) B
wk+ Γ Δ Θ t = t [ wk↑ {Γ} Δ Θ ]

sub+ : (Γ Θ : Con) → Tm (Γ + Δ + Θ) B → Sub Γ Δ → Tm (Γ + Θ) B
sub+ {Δ} {B} Γ Θ t σ = t [ sub↑ {Γ} Δ Θ σ ]

syntax wk+ Γ Δ Θ t = t ⟨ Γ , Δ , Θ ⟩+
syntax sub+ Γ Θ t σ = t ⌈ Γ , σ , Θ ⌉+

equ[⌉ : wk↑ {∙} Γ Δ ∘ sub↑ {Γ} Δ ∙ σ ≡ σ
equ[⌉ {Γ} {∙} {σ}     = begin
 wk↑ Γ ∙ ∘ id ≡⟨ ∙η {σ = wk↑ Γ ∙ ∘ id} ⟩
 ε ≡⟨ sym (∙η {σ = σ}) ⟩
 σ ∎
equ[⌉ {∙} {Δ ▷ A} {σ} = begin
  id ∘ (sub↑ Δ ∙ (p ∘ σ) , q [ σ ]) ≡⟨ idl {σ = sub↑ Δ ∙ (p ∘ σ) , q [ σ ]} ⟩
  sub↑ Δ ∙ (p ∘ σ) , q [ σ ]        ≡⟨ cong (λ x → x , q [ σ ]) (sym (idl {σ = sub↑ Δ ∙ (p ∘ σ)})) ⟩
  (id ∘ sub↑ Δ ∙ (p ∘ σ)) , q [ σ ] ≡⟨ cong (λ x → x , q [ σ ]) (equ[⌉ {∙}{Δ}{p ∘ σ}) ⟩
  p ∘ σ , q [ σ ]                   ≡⟨ sym (,∘ {σ = p}{t = q}{δ = σ}) ⟩
  (p , q) ∘ σ                       ≡⟨ cong (λ x → x ∘ σ) ▷η ⟩
  id ∘ σ                            ≡⟨ idl {σ = σ} ⟩
  σ
  ∎
equ[⌉ {Γ ▷ B} {Δ ▷ A} {σ} = begin
  (wk↑ (Γ ▷ B) Δ ∘ p , q) ∘ (sub↑ Δ ∙ (p ∘ σ) , q [ σ ])
    ≡⟨ cong (λ x → (wk↑ {∙}(Γ ▷ B) Δ ∘ p , q) ∘ (x , q [ σ ])) (sym (idr {σ = sub↑ Δ ∙ (p ∘ σ)})) ⟩
  (wk↑ {∙}(Γ ▷ B) Δ ∘ p , q) ∘ (sub↑ Δ ∙ (p ∘ σ) ∘ id , q [ σ ])
    ≡⟨ ∘p,q∘ {σ = wk↑ (Γ ▷ B) Δ}{δ = sub↑ Δ ∙ (p ∘ σ)}{ν = id}{u = q [ σ ]} ⟩
  (wk↑ {∙}(Γ ▷ B) Δ ∘ sub↑ Δ ∙ (p ∘ σ)) ∘ id , q [ σ ]
    ≡⟨ cong (λ x → x ∘ id , q [ σ ]) (equ[⌉ {Γ ▷ B}{Δ}{p ∘ σ}) ⟩
  (p ∘ σ) ∘ id , q [ σ ]
    ≡⟨ cong (λ x → x , q [ σ ]) (idr {σ = p ∘ σ}) ⟩
  (p ∘ σ) , q [ σ ]
    ≡⟨ sym (,∘ {σ = p}{t = q}{δ = σ}) ⟩
  (p , q) ∘ σ
    ≡⟨ cong (λ x → x ∘ σ) ▷η ⟩
  id ∘ σ
    ≡⟨ idl {σ = σ} ⟩
  σ
  ∎

equ[] : t [ σ ] ≡ t ⟨ ∙ , Γ , Δ ⟩+ ⌈ Γ , σ , ∙ ⌉+
equ[] {_} {Δ} {t} {Γ} {σ} = begin
  t [ σ ] ≡⟨ cong (λ x → t [ x ]) (sym (equ[⌉ {Γ}{Δ}{σ})) ⟩
  t [ wk↑ {∙} Γ Δ ∘ sub↑ Δ ∙ σ ] ≡⟨ [∘] {t = t} ⟩
  t [ wk↑ {∙} Γ Δ ] [ sub↑ Δ ∙ σ ] ∎

-- iso

data Tms : Con → Con → Set where
  ε' : Tms Γ ∙
  _,'_ : Tms Γ Δ → Tm Γ A → Tms Γ (Δ ▷ A)
variable
  ξ ρ : Tms Γ Δ

h : Tms Γ Δ → Sub Γ Δ
h ε' = ε
h (σ ,' u) = h σ , u

h̅ : Sub Γ Δ → Tms Γ Δ
h̅ {Γ} {∙} σ = ε'
h̅ {Γ} {Δ ▷ A} σ =  h̅ (p ∘ σ) ,' (q [ σ ])

hβ : h̅ (h ξ) ≡ ξ
hβ {Γ} {.∙} {ε'} = refl
hβ {Γ} {Δ ▷ A} {ξ ,' u} = begin
  h̅ (p ∘ (h ξ , u)) ,' q [ h ξ , u ] ≡⟨ cong (λ x → h̅ x ,' q [ h ξ , u ]) ▷β₁ ⟩
  h̅ (h ξ) ,' q [ h ξ , u ] ≡⟨ cong (λ x → x ,' q [ h ξ , u ] ) hβ  ⟩
  ξ ,' q [ h ξ , u ] ≡⟨ cong (λ x → ξ ,' x ) ▷β₂ ⟩
  ξ ,' u ∎

hη : h (h̅ σ) ≡ σ
hη {Γ} {∙} {σ} = sym ∙η 
hη {Γ} {Δ ▷ x} {σ} = begin
  h (h̅ (p ∘ σ)) , q [ σ ] ≡⟨ cong (λ x → x , q [ σ ]) hη ⟩
  p ∘ σ , q [ σ ]         ≡⟨ sym ,∘ ⟩
  (p , q) ∘ σ             ≡⟨ cong (λ x → x ∘ σ) ▷η ⟩
  id ∘ σ                  ≡⟨ idl ⟩
  σ ∎

sub+' : (Γ Θ : Con) → Tm (Γ + Δ + Θ) B → Tms Γ Δ → Tm (Γ + Θ) B
sub+'         _ _ t ε'       = t
sub+' {Δ ▷ A} Γ Θ t (σ ,' u) = sub+' Γ (∙ ▷ A + Θ) t σ ⌈ Γ , u , Θ ⌉

syntax sub+' Γ Θ t σ = t ⌈ Γ , σ , Θ ⌉+'

_[_]t : Tm Δ A → Tms Γ Δ → Tm Γ A
_[_]t {Δ}{A}{Γ} t ξ = t ⟨ ∙ , Γ , Δ ⟩+ ⌈ Γ , ξ , ∙ ⌉+'

wks : (Γ : Con) → (A : Ty) → (Θ : Con) → Tms (Γ + Θ) Δ → Tms (Γ ▷ A + Θ) Δ
wks Γ A Θ ε'       = ε'
wks Γ A Θ (σ ,' u) = wks Γ A Θ σ ,' u ⟨ Γ , A , Θ ⟩

wks+ : (Γ Ω Θ : Con) → Tms (Γ + Θ) Δ → Tms (Γ + Ω + Θ) Δ
wks+ Γ Ω Θ ε'       = ε'
wks+ Γ Ω Θ (σ ,' u) = wks+ Γ Ω Θ σ ,' u ⟨ Γ , Ω , Θ ⟩+

syntax wks Γ A Θ σ = σ ⟨ Γ , A , Θ ⟩s
syntax wks+ Γ Ω Θ σ = σ ⟨ Γ , Ω , Θ ⟩s+


id' : Tms Γ Γ
id' {∙}     = ε'
id' {Γ ▷ A} = (id' {Γ} ⟨ Γ , A , ∙ ⟩s ) ,' q

_∘'_ : Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
ε'       ∘' δ = ε'
(σ ,' u) ∘' δ = σ ∘' δ ,' u [ δ ]t

p' : Tms (Γ ▷ A) Γ
p' {Γ}{A} = id' {Γ} ⟨ Γ , A , ∙ ⟩s


⟨∘⟩s : ξ ∘' p' ≡ ξ ⟨ Γ , C , ∙ ⟩s
⟨∘⟩s = {!!} --proved in tel.agda

hε : h {Γ} ε' ≡ ε
hε = refl

h, : {ξ : Tms Γ Δ} → h (ξ ,' u) ≡ h ξ , u
h, = refl

h∘ : {ξ : Tms Θ Δ} → {ρ : Tms Γ Θ} → h (ξ ∘' ρ) ≡ h ξ ∘ h ρ
h∘ {Θ} {.∙} {Γ} {ε'} {ρ} = sym ∙η
h∘ {Θ} {Δ ▷ A} {Γ} {ξ ,' u} {ρ} = begin
  h (ξ ∘' ρ) , (u [ ρ ]t) ≡⟨ cong (λ x → x , u [ ρ ]t ) (h∘ {ξ = ξ}{ρ = ρ}) ⟩
  h ξ ∘ h ρ , u [ ρ ]t ≡⟨ {!,∘ {σ = h ξ}{δ = h ρ}!} ⟩
  (h ξ , u) ∘ h ρ  ∎

hp : h (p' {Γ}{A}) ≡ p
hp {∙} {A} = sym ∙η
hp {Γ ▷ B} {A} = begin
  h (p' ⟨ Γ ▷ B , A , ∙ ⟩s ) , q ⟨ Γ ▷ B , A , ∙ ⟩  ≡⟨ {!!} ⟩
  h (p' ∘' p' ) , q ⟨ Γ ▷ B , A , ∙ ⟩ ≡⟨ {!!} ⟩
  p ∎
  
hid : h (id' {Γ}) ≡ id
hid {∙} = sym ∙η
hid {Γ ▷ A} = begin
  h ( id' ⟨ Γ , A , ∙ ⟩s ) , q ≡⟨ cong (λ x → h x , q ) (sym ⟨∘⟩s) ⟩
  h ( id' ∘' p' ) , q ≡⟨ {!idl'!} ⟩
  h ( p' ) , q ≡⟨ cong (λ x → x , q) hp ⟩
  p , q ≡⟨ ▷η ⟩
  id ∎
  

